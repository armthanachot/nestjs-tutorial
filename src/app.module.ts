import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from "@nestjs/common";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Connection } from "typeorm";
import { User } from "./user/user.entity";
import { LifestyleImage } from "./lifestyle/lifestyle.entity";
import { UserModule } from "./user/user.module";
import { verifyToken } from "../middleware/auth.middle";
import { ROLES } from "../constants/config";
import { LifestyleModule } from './lifestyle/lifestyle.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: "mysql",
      host: "127.0.0.1",
      port: 3306,
      username: "root",
      password: "123456",
      database: "space_station",
      entities: [User,LifestyleImage],
      synchronize: true,
      keepConnectionAlive:true
    }),
    UserModule,
    LifestyleModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  constructor(private connection: Connection) {}
  async configure(consumer: MiddlewareConsumer) {
    const { GET, POST, PUT, DELETE, ALL } = RequestMethod;
    const { ADMIN, DEV, USER } = ROLES;
    consumer.apply(await verifyToken([ADMIN, DEV, USER])).forRoutes({
      path: "user",
      method: GET,
    });
    consumer.apply(await verifyToken([ADMIN, DEV, USER])).forRoutes({
      path: "user/:id",
      method: GET,
    });
  }
}
