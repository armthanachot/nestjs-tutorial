import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as express from 'express';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use('/static', express.static('static'));
  app.enableCors()
  await app.listen(3300);
}
bootstrap();
