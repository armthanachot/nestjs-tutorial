import { Entity, Column, PrimaryGeneratedColumn,CreateDateColumn,UpdateDateColumn,OneToOne,JoinColumn,ManyToOne} from 'typeorm';
import {User} from "../user/user.entity"
@Entity()
export class LifestyleImage{
    @PrimaryGeneratedColumn()
    id: number;
    
    @ManyToOne(type => User,{primary:true,onDelete:"CASCADE",onUpdate:"CASCADE"}) // fk จาก User_type โดย reference จาก primary ของ User_type 
    @JoinColumn({referencedColumnName:"id"})
    user:User

    @CreateDateColumn()
    created_at:Date

    @UpdateDateColumn()
    updated_at:Date
}