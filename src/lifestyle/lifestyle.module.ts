import { Module } from '@nestjs/common';
import { LifestyleService } from './lifestyle.service';
import {LifestyleController} from "./lifestyle.controller"
import {LifestyleImage} from "./lifestyle.entity"
import {TypeOrmModule} from "@nestjs/typeorm"

@Module({
  imports:[TypeOrmModule.forFeature([LifestyleImage])],
  controllers:[LifestyleController],
  providers: [LifestyleService]
})
export class LifestyleModule {}
