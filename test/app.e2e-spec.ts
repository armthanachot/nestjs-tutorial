import { Test, TestingModule } from "@nestjs/testing";
import { INestApplication } from "@nestjs/common";
import * as request from "supertest";
import { AppModule } from "./../src/app.module";
import * as dotenv from "dotenv";
dotenv.config();

describe("AppController (e2e)", () => {
  let app: INestApplication;
  let token = null;
  // beforeEach() ก็คือสิ่งที่จะให้ทำทุกครั้งก่อนที่จะไปทำแต่ละ test case
  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it("/user/login", async (done) => {
    const account = {
      email: process.env.EMAIL,
      password: process.env.PASSWORD,
    };
    const result = await request(app.getHttpServer()).post("/user/login").set(
      "Accept",
      "application/json",
    ).send(account).expect(200);
    token = result.body.data;
    done();
  });

  it("/user (GET)", async (done) => {
    const result = await request(app.getHttpServer()).get("/user")
      .set("Authorization", `Bearer ${token}`)
      .expect(200);
    done();
  });

  it("/user/:id (GET)", async (done) => {
    const result = await request(app.getHttpServer()).get("/user/2").set(
      "Authorization",
      `Bearer ${token}`,
    ).expect(200);
    console.log(result.body.data);
    done()
  });
  afterAll((done) => {
    app.close();
    done();
  });
});
