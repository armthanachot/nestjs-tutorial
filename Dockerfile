FROM node:12.18-alpine
WORKDIR /usr/src/app
COPY ["package.json", "./"]
RUN npm install -g @nestjs/cli
RUN npm install
COPY . .
EXPOSE 3300
CMD ["npm", "start"]
